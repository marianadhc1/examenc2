function quitar(){
    const origen = document.getElementById('monedaOrigen');
    const destino = document.getElementById('monedaDestino');
    let selectO = ""; 
    let selectD = "";

//5 peso mexicano
//6 dolar estadounidense
//7 dolar canadiense
//8 euro

    if(parseInt(origen.value)==1) //5 Peso mexicano
    {
        selectD = 
            `<option value="6">Dolar estadounidense</option>
            <option value="7">Dolar canadiense</option>
            <option value="8">Euro</option>`
        
        selectO = 
            `<option value="1" selected>Peso mexicano</option>
            <option value="2">Dolar estadounidense</option>
            <option value="3">Dolar canadiense</option>
            <option value="4">Euro</option>`
    }
    if(parseInt(origen.value)==2) //6 Dolar estadounidense
    {
        selectD = 
            `<option value="5">Peso mexicano</option>
            <option value="7">Dolar canadiense</option>
            <option value="8">Euro</option>`
        
        selectO = 
            `<option value="1">Peso mexicano</option>
            <option value="2" selected>Dolar estadounidense</option>
            <option value="3">Dolar canadiense</option>
            <option value="4">Euro</option>`
    }
    if(parseInt(origen.value)==3) //7 Dolar canadiense
    {
        selectD = 
            `<option value="5">Peso mexicano</option>
            <option value="6">Dolar estadounidense</option>
            <option value="8">Euro</option>`
        
        selectO = 
            `<option value="1">Peso mexicano</option>
            <option value="2">Dolar estadounidense</option>
            <option value="3" selected>Dolar canadiense</option>
            <option value="4">Euro</option>`
    }
    if(parseInt(origen.value)==4) //8 Euro
    {
        selectD = 
            `<option value="5">Peso mexicano</option>
            <option value="6">Dolar estadounidense</option>
            <option value="7">Dolar canadiense</option>`
        
        selectO = 
            `<option value="1">Peso Mexicano</option>
            <option value="2">Dolar estadounidense</option>
            <option value="3">Dolar canadiense</option>
            <option value="4" selected>Euro</option>`
    }

    origen.innerHTML = selectO;
    destino.innerHTML = selectD;
}

function calcular(){
    //1/ 5 peso mexicano
    //2/ 6 dolar estadounidense
    //3/ 7 dolar canadiense
    //4/ 8 euro

    let cantidad = document.getElementById('cantidad').value;
    let origen = document.getElementById('monedaOrigen');
    let destino = document.getElementById('monedaDestino');
    let subtotal = document.getElementById('subtotal');
    let comision = document.getElementById('comision');
    let totalPagar = document.getElementById('pagar');

    //Pesos mexicanos
    if(parseInt(origen.value)==1 && parseInt(destino.value)==6){
        subtotal.value = (cantidad/19.85).toFixed(2);
    }else if(parseInt(origen.value)==1 && parseInt(destino.value)==7){
        subtotal.value = ((cantidad/19.85)*1.35).toFixed(2);
    }else if(parseInt(origen.value)==1 && parseInt(destino.value)==8){
        subtotal.value = ((cantidad/19.85)*0.99).toFixed(2);
    }

    //Dolares estadounidense
    else if(parseInt(origen.value)==2 && parseInt(destino.value)==5){
        subtotal.value = (cantidad*19.85).toFixed(2);
    }else if(parseInt(origen.value)==2 && parseInt(destino.value)==7){
        subtotal.value = (cantidad*1.35).toFixed(2);
    }else if(parseInt(origen.value)==2 && parseInt(destino.value)==8){
        subtotal.value = (cantidad*0.99).toFixed(2);
    }

    //Dolares canadiense
    else if(parseInt(origen.value)==3 && parseInt(destino.value)==5){
        subtotal.value = ((cantidad/1.35)*19.85).toFixed(2);
    }else if(parseInt(origen.value)==3 && parseInt(destino.value)==6){
        subtotal.value = (cantidad/1.35).toFixed(2);
    }else if(parseInt(origen.value)==3 && parseInt(destino.value)==8){
        subtotal.value =  ((cantidad/1.35)*0.99).toFixed(2);
    }

    //Euro
    else if(parseInt(origen.value)==4 && parseInt(destino.value)==5){
        subtotal.value = ((cantidad/0.99)*19.85).toFixed(2);
    }else if(parseInt(origen.value)==4 && parseInt(destino.value)==6){
        subtotal.value = (cantidad/0.99).toFixed(2);
    }else if(parseInt(origen.value)==4 && parseInt(destino.value)==7){
        subtotal.value =  ((cantidad/0.99)*1.35).toFixed(2);
    }

    comision.value = (subtotal.value *0.03).toFixed(2);
    totalPagar.value = (parseFloat(subtotal.value) + parseFloat(comision.value)).toFixed(2);

    console.log(totalPagar.value);
}

let subF=0, comF=0, totF=0;
function registrar(){
    let cantidad = document.getElementById('cantidad');
    let origen = document.getElementById('monedaOrigen');
    let destino = document.getElementById('monedaDestino');
    let subtotal = document.getElementById('subtotal');
    let comision = document.getElementById('comision');
    let totalPagar = document.getElementById('pagar');

    let registros = document.getElementById('registros');
    let registrosTotales = document.getElementById('totales');

    //200 Pesos mexicanos a dolar canadiense 999.999 999.9 999.9
    registros.innerText = registros.innerText + 
        `$${cantidad.value} en moneda ${origen.value} son $${subtotal.value} en moneda ${destino.value} con una comisión de ${comision.value} y un total a pagar de ${totalPagar.value}`;

    subF = subF + parseFloat(subtotal.value);
    comF = comF + parseFloat(comision.value);
    totF = totF + parseFloat(totalPagar.value);

    registrosTotales.innerText = registros.innerText;
        `Subtotal: ${parseFloat(subF)} | Comisión: ${parseFloat(comF)} | Total: ${parseFloat(totF)}`;
}

function borrar(){
    let cantidad = document.getElementById('cantidad');
    let subtotal = document.getElementById('subtotal');
    let comision = document.getElementById('comision');
    let totalPagar = document.getElementById('pagar');

    let registros = document.getElementById('registros');
    let registrosTotales = document.getElementById('totales');
    
    cantidad.innerHTML = '';
    subtotal.innerHTML = '';
    comision.innerHTML = '';
    totalPagar.innerHTML = '';
    registros.innerHTML = '';
    registrosTotales.innerHTML = '';

    subF=0;
    pagarF=0;
    comF=0;
}